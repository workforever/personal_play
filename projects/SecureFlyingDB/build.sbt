name := "SecureFlyingDB"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)
     
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.27"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.0"

libraryDependencies +=  "be.objectify" %% "deadbolt-java" % "2.2-RC4"

libraryDependencies += "com.feth" %% "play-authenticate" % "0.5.2-SNAPSHOT"

// The Typesafe repository
resolvers ++= Seq(
		Resolver.url("Objectify Play Repository (release)", url("http://schaloner.github.com/releases/"))(Resolver.ivyStylePatterns),
		Resolver.url("Objectify Play Repository (snapshot)", url("http://schaloner.github.com/snapshots/"))(Resolver.ivyStylePatterns),
		Resolver.url("play-easymail (release)", url("http://joscha.github.com/play-easymail/repo/releases/"))(Resolver.ivyStylePatterns),
		Resolver.url("play-easymail (snapshot)", url("http://joscha.github.com/play-easymail/repo/snapshots/"))(Resolver.ivyStylePatterns),
		Resolver.url("play-authenticate (release)", url("http://joscha.github.com/play-authenticate/repo/releases/"))(Resolver.ivyStylePatterns),
		Resolver.url("play-authenticate (snapshot)", url("http://joscha.github.com/play-authenticate/repo/snapshots/"))(Resolver.ivyStylePatterns),
        "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"
)


play.Project.playJavaSettings
