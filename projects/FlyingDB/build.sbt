name := "FlyingDB"

version := "1.0-SNAPSHOT"

libraryDependencies ++= Seq(
  javaJdbc,
  javaEbean,
  cache
)
     
libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.27"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.0"

play.Project.playJavaSettings
