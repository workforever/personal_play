package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

import com.avaje.ebean.Page;

/**
 * Professor entity managed by Ebean
 */

@Entity 
public class Professor  extends Model  {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @ManyToOne
    public Department department;

	@Constraints.Required
    public String name;
    
    @Constraints.Required
    public String title;

    @Constraints.Required
    public String webUrl;
    
    @Constraints.Required
    public String researchArea;

    @Constraints.Required
    public String cv;
    
    @Constraints.Required
    public String publication;
    
    /**
     * Generic query helper for entity University with id Long
     */
    public static Finder<Long,Professor> find = new Finder<Long,Professor>(Long.class, Professor.class); 
    
    /**
     * Return a page of Professor
     *
     * @param page Page to display
     * @param pageSize Number of Professors per page
     * @param sortBy Professor property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<Professor> page(Long did, int page, int pageSize, String sortBy, String order, String filter) {
        return 
            find.where()
            	.ieq("department.id", Long.toString(did))
                .ilike("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .fetch("department")
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
}
