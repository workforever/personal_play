package models;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Page;

import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

/**
 * Department entity managed by Ebean
 */

@Entity 
public class Department  extends Model {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @ManyToOne
    public University university;

	@Constraints.Required
    public String name;
    
    @Constraints.Required
    public Long rank;

    @Constraints.Required
    public String webUrl;
    
    @Constraints.Required
    public String graduateProgram;

    @Constraints.Required
    public String graduateProgramWebUrl;
    
    /**
     * Generic query helper for entity University with id Long
     */
    public static Finder<Long,Department> find = new Finder<Long,Department>(Long.class, Department.class); 
    
    /**
     * Return a page of University
     *
     * @param page Page to display
     * @param pageSize Number of universities per page
     * @param sortBy University property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<Department> page(Long uid, int page, int pageSize, String sortBy, String order, String filter, int rankfilter) {
        return 
            find.where()
            	.le("rank",rankfilter)
            	.ieq("university.id", Long.toString(uid))
                .ilike("name", "%" + filter + "%")
                .orderBy(sortBy + " " + order)
                .fetch("university")
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
}
