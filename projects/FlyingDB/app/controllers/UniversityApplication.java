package controllers;

import static play.data.Form.form;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.createUniversityForm;
import views.html.editUniversityForm;
import views.html.universityList;
import models.*;

import org.apache.commons.lang3.*;
import org.apache.commons.lang3.math.*;


/**
 * Manage a database of Universities.
 */
public class UniversityApplication  extends Controller  {
    /**
     * This result directly redirect to application home.
     */
    public static Result GO_HOME = redirect(
        routes.UniversityApplication.list(0, "rank", "asc", "","")
    );
    
    /**
     * Handle default path requests, redirect to universities list
     */
    public static Result index() {
        return GO_HOME;
    }

    /**
     * Display the paginated list of universities.
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on university names
     */
    public static Result list(int page, String sortBy, String order, String namefilter, String rankfilter) {
    	int rankFilter =  NumberUtils.toInt(rankfilter);
    	if(rankFilter<=0) {
    		rankFilter = 10000;
    	}
        return ok(
            universityList.render(
                University.page(page, 10, sortBy, order, namefilter, rankFilter),
                sortBy, order, namefilter, rankfilter
            )
        );
    }
    
    /**
     * Display the 'edit form' of a existing Computer.
     *
     * @param id Id of the University to edit
     */
    public static Result edit(Long id) {
        Form<University> universityForm = form(University.class).fill(
        		University.find.byId(id)
        );
        return ok(
            editUniversityForm.render(id, universityForm)
        );
    }
    
    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the University to edit
     */
    public static Result update(Long id) {
        Form<University> universityForm = form(University.class).bindFromRequest();
        if(universityForm.hasErrors()) {
            return badRequest(editUniversityForm.render(id, universityForm));
        }
        universityForm.get().update(id);
        flash("success", "University " + universityForm.get().name + " has been updated");
        return GO_HOME;
    }
    
    /**
     * Display the 'new university form'.
     */
    public static Result create() {
        Form<University> universityForm = form(University.class);
        return ok(
            createUniversityForm.render(universityForm)
        );
    }
    
    /**
     * Handle the 'new university form' submission 
     */
    public static Result save() {
        Form<University> universityForm = form(University.class).bindFromRequest();
        if(universityForm.hasErrors()) {
            return badRequest(createUniversityForm.render(universityForm));
        }
//        System.out.println("chinese name:"+universityForm.get().chineseName);
        universityForm.get().save();
        flash("success", "University " + universityForm.get().name + " has been created");
        return GO_HOME;
    }
    
    /**
     * Handle university deletion
     */
    public static Result delete(Long id) {
    	University.find.ref(id).delete();
        flash("success", "University has been deleted");
        return GO_HOME;
    }

}
