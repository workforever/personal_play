# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table department (
  id                        bigint auto_increment not null,
  university_id             bigint,
  name                      varchar(255),
  rank                      bigint,
  web_url                   varchar(255),
  graduate_program          varchar(255),
  graduate_program_web_url  varchar(255),
  constraint pk_department primary key (id))
;

create table professor (
  id                        bigint auto_increment not null,
  department_id             bigint,
  name                      varchar(255),
  title                     varchar(255),
  web_url                   varchar(255),
  research_area             varchar(255),
  cv                        varchar(255),
  publication               varchar(255),
  constraint pk_professor primary key (id))
;

create table university (
  id                        bigint auto_increment not null,
  name                      varchar(255),
  chinese_name              varchar(255),
  rank                      bigint,
  abbreviation              varchar(255),
  location                  varchar(255),
  web_url                   varchar(255),
  wiki_url                  varchar(255),
  constraint pk_university primary key (id))
;

alter table department add constraint fk_department_university_1 foreign key (university_id) references university (id) on delete restrict on update restrict;
create index ix_department_university_1 on department (university_id);
alter table professor add constraint fk_professor_department_2 foreign key (department_id) references department (id) on delete restrict on update restrict;
create index ix_professor_department_2 on professor (department_id);



# --- !Downs

SET FOREIGN_KEY_CHECKS=0;

drop table department;

drop table professor;

drop table university;

SET FOREIGN_KEY_CHECKS=1;

