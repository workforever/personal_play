package models.flyingdb;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Page;

import play.data.format.Formats;
import play.data.validation.Constraints;
import play.db.ebean.Model;
import play.db.ebean.Model.Finder;

/**
 * University entity managed by Ebean
 */

@Entity 
public class University extends Model  {

    private static final long serialVersionUID = 1L;

	@Id
    public Long id;
    
    @Constraints.Required
    public String name;
    
    @Constraints.Required
    public String chineseName;
    
    @Constraints.Required
    public Long rank;

	@Constraints.Required
    public String abbreviation;
    
    @Constraints.Required
    public String location;
    
    @Constraints.Required
    public String webUrl;
    
    @Constraints.Required
    public String wikiUrl;
    
    /**
     * Generic query helper for entity University with id Long
     */
    public static Finder<Long,University> find = new Finder<Long,University>(Long.class, University.class); 
    
    /**
     * Return a page of University
     *
     * @param page Page to display
     * @param pageSize Number of universities per page
     * @param sortBy University property used for sorting
     * @param order Sort order (either or asc or desc)
     * @param filter Filter applied on the name column
     */
    public static Page<University> page(int page, int pageSize, String sortBy, String order, String namefilter, int rankfilter) {
        return 
            find.where()
            	.le("rank",rankfilter)
                .ilike("name", "%" + namefilter + "%")
                .orderBy(sortBy + " " + order)
                .findPagingList(pageSize)
                .setFetchAhead(false)
                .getPage(page);
    }
}
