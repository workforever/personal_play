package controllers.flyingdb;

import static play.data.Form.form;

import org.apache.commons.lang3.math.NumberUtils;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.flyingdb.*;
import models.flyingdb.*;

@Restrict(@Group("admin"))
public class DepartmentApplication extends Controller  {

	/**
     * Display the paginated list of departments.
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on department names
     */
    public static Result list(Long uid, int page, String sortBy, String order, String filter, String rankfilter) {
    	int rankFilter =  NumberUtils.toInt(rankfilter);
    	if(rankFilter<=0) {
    		rankFilter = 10000;
    	}
        return ok(
            departmentList.render(uid,University.find.byId(uid),
                Department.page(uid, page, 10, sortBy, order, filter, rankFilter),
                sortBy, order, filter, rankfilter
            )
        );
    }
    
    /**
     * Display the 'edit form' of a existing Department.
     *
     * @param id Id of the Department to edit
     */
    public static Result edit(Long uid, Long id) {
        Form<Department> departmentForm = form(Department.class).fill(
        		Department.find.byId(id)
        );
        return ok(
            editDepartmentForm.render(uid,id, departmentForm)
        );
    }
    
    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the department to edit
     */
    public static Result update(Long uid, Long id) {
        Form<Department> departmentForm = form(Department.class).bindFromRequest();
        if(departmentForm.hasErrors()) {
            return badRequest(editDepartmentForm.render(uid, id, departmentForm));
        }
        departmentForm.get().university = University.find.byId(uid);
        departmentForm.get().update(id);
        flash("success", "Department " + departmentForm.get().name + " has been updated");
        return redirect(
                routes.DepartmentApplication.list(uid, 0, "rank", "asc", "", "")
        	    );
    }
    
    /**
     * Display the 'new department form'.
     */
    public static Result create(Long uid) {
        Form<Department> departmentForm = form(Department.class);
        return ok(
            createDepartmentForm.render(uid, departmentForm)
        );
    }
    
    /**
     * Handle the 'new department form' submission 
     */
    public static Result save(Long uid) {
        Form<Department> departmentForm = form(Department.class).bindFromRequest();
        if(departmentForm.hasErrors()) {
            return badRequest(createDepartmentForm.render(uid, departmentForm));
        }
        departmentForm.get().university = University.find.byId(uid);
        departmentForm.get().save();
        flash("success", "Department " + departmentForm.get().name + " has been created");
        return redirect(
                routes.DepartmentApplication.list(uid, 0, "rank", "asc", "", "")
        	    );
    }
    
    /**
     * Handle department deletion
     */
    public static Result delete(Long uid, Long id) {
    	Department.find.ref(id).delete();
        flash("success", "Department has been deleted");
        return redirect(
                routes.DepartmentApplication.list(uid, 0, "rank", "asc", "", "")
        	    );
    }    
    

}
