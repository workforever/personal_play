package controllers.flyingdb;

import static play.data.Form.form;

import org.apache.commons.lang3.math.NumberUtils;

import be.objectify.deadbolt.java.actions.Group;
import be.objectify.deadbolt.java.actions.Restrict;
import play.data.Form;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.flyingdb.*;
import models.flyingdb.*;

@Restrict(@Group("admin"))
public class ProfessorApplication  extends Controller {

	/**
     * Display the paginated list of professors.
     *
     * @param page Current page number (starts from 0)
     * @param sortBy Column to be sorted
     * @param order Sort order (either asc or desc)
     * @param filter Filter applied on professor names
     */
    public static Result list(Long uid, Long did, int page, String sortBy, String order, String filter) {
        return ok(
            professorList.render(did,Department.find.byId(did),
                Professor.page(did, page, 10, sortBy, order, filter),
                sortBy, order, filter
            )
        );
    }
    
    /**
     * Display the 'edit form' of a existing Professor.
     *
     * @param id Id of the Professor to edit
     */
    public static Result edit(Long uid, Long did, Long id) {
        Form<Professor> professorForm = form(Professor.class).fill(
        		Professor.find.byId(id)
        );
        return ok(
            editProfessorForm.render(uid,did,id, professorForm)
        );
    }
    
    /**
     * Handle the 'edit form' submission 
     *
     * @param id Id of the professor to edit
     */
    public static Result update(Long uid, Long did, Long id) {
        Form<Professor> professorForm = form(Professor.class).bindFromRequest();
        if(professorForm.hasErrors()) {
            return badRequest(editProfessorForm.render(uid, did, id, professorForm));
        }
        professorForm.get().department = Department.find.byId(did);
        professorForm.get().update(id);
        flash("success", "Professor " + professorForm.get().name + " has been updated");
        return redirect(
                routes.ProfessorApplication.list(uid,did, 0, "name", "asc", "")
        	    );
    }
    
    /**
     * Display the 'new professor form'.
     */
    public static Result create(Long uid, Long did) {
        Form<Professor> professorForm = form(Professor.class);
        return ok(
            createProfessorForm.render(uid, did, professorForm)
        );
    }
    
    /**
     * Handle the 'new Professor form' submission 
     */
    public static Result save(Long uid, Long did) {
        Form<Professor> professorForm = form(Professor.class).bindFromRequest();
        if(professorForm.hasErrors()) {
            return badRequest(createProfessorForm.render(uid, did, professorForm));
        }
        professorForm.get().department = Department.find.byId(did);
        professorForm.get().save();
        flash("success", "Professor " + professorForm.get().name + " has been created");
        return redirect(
                routes.ProfessorApplication.list(uid,did, 0, "name", "asc", "")
        	    );
    }
    
    /**
     * Handle professor deletion
     */
    public static Result delete(Long uid, Long did, Long id) {
    	Professor.find.ref(id).delete();
        flash("success", "Professor has been deleted");
        return redirect(
                routes.ProfessorApplication.list(uid,did, 0, "name", "asc", "")
        	    );
    }    

}
